﻿namespace KeyPress.Wrappers
{
    public class KeyboardInputSender : IInputSender 
    {
        public void SendKey(ScanCodeShort scanCodeShort)
        {
            INPUT[] Inputs = new INPUT[1];
            INPUT Input = new INPUT();
            Input.type = 1; // 1 = Keyboard Input
            Input.U.ki.wScan = scanCodeShort;
            Input.U.ki.dwFlags = KEYEVENTF.SCANCODE;
            Inputs[0] = Input;
            User32Wrapper.SendInput(1, Inputs, INPUT.Size);
        }

        public void SendKey(VirtualKeyShort virtualKeyShort)
        {
            INPUT[] Inputs = new INPUT[1];
            INPUT Input = new INPUT();
            Input.type = 1; // 1 = Keyboard Input
            Input.U.ki.wVk= virtualKeyShort;
            Inputs[0] = Input;
            User32Wrapper.SendInput(1, Inputs, INPUT.Size);

            Inputs = new INPUT[1];
            Input = new INPUT();
            Input.type = 1; // 1 = Keyboard Input
            Input.U.ki.wVk = virtualKeyShort;
            Input.U.ki.dwFlags = KEYEVENTF.KEYUP;
            Inputs[0] = Input;
            User32Wrapper.SendInput(1, Inputs, INPUT.Size);
        }
    }

}
