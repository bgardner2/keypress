﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyPress.Wrappers
{
    public interface IInputSender
    {

    }

    public class MouseInputSender
    {
        public void MoveMouse(int x, int y)
        {
            INPUT[] Inputs = new INPUT[1];
            INPUT Input = new INPUT();
            Input.type = 2; // 1 = Mouse Input
            Input.U.mi.dx = x;
            Input.U.mi.dy = y;
            Input.U.mi.mouseData = 0;
            Input.U.mi.dwFlags = MOUSEEVENTF.MOVE;
            Inputs[0] = Input;
            User32Wrapper.SendInput(1, Inputs, INPUT.Size);
        }
    }
}
