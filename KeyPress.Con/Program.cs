﻿using KeyPress.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace KeyPress.Con
{
    class Program
    {
        private static KeyboardInputSender keySender;
        static void Main(string[] args)
        {
            Timer t = new Timer(60000);
            t.Elapsed += T_Elapsed;
            t.Start();

            keySender = new KeyboardInputSender();
            
            Console.WriteLine($"Done");
            Console.ReadKey();
            t.Stop();

        }

        private static void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            keySender.SendKey(VirtualKeyShort.NUMLOCK);
        }
    }
}
