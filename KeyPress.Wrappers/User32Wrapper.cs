﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KeyPress.Wrappers
{
    internal static class User32Wrapper
    {
        /// <summary>
        /// This method uses the Win32 Api to send an input such as mouse, keyboard, or hardware to the system.
        /// </summary>
        /// <param name="nInputs">The number of structures in the pInputs array.</param>
        /// <param name="pInputs">An array of <see cref="INPUT"/> structures. Each structure represents an event to be inserted into the keyboard or mouse input stream</param>
        /// <param name="cbSize">The size, in bytes, of an INPUT structure. If cbSize is not the size of an INPUT structure, the function fails</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        internal static extern uint SendInput(
            uint nInputs,
            [MarshalAs(UnmanagedType.LPArray), In] INPUT[] pInputs,
            int cbSize);

    }
}
